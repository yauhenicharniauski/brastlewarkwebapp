import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PeopleTemplate from './PeopleTemplate';
import { peopleFetchAsync } from '../../js/actions/people';
import peopleFilterHelper from '../../js/utils/peopleFilterHelper';
import PeopleList from './PeopleList';
import Loading from '../../components/Loading/Loading';
import Error from '../../components/Error/Error';

import './People.scss';

/*
* we keep link to items in state object,
* because we don't want call filter each time when getDerivedStateFromProps exec
* We just need compare state.items with props.items to avoid extra filtering
*/

const initialState = {
  filterText: '',
  items: [],
  filteredItems: [],
};

class PeopleScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { ...initialState };
    this.handleFilter = this.handleFilter.bind(this);
  }

  componentDidMount() {
    this.props.peopleFetch();
  }

  static getDerivedStateFromProps(props, state) {
    if (props.items === state.items) {
      return null;
    }

    return {
      items: props.items,
      filteredItems: peopleFilterHelper.filterPeopleByName(props.items, state.filterText),
    };
  }

  getContent() {
    if (this.props.inProgress) {
      return <Loading />;
    } if (this.props.error) {
      return <Error />;
    }
    return <PeopleList items={this.state.filteredItems} />;
  }

  handleFilter(event) {
    const filterText = event.target.value;
    const filteredItems = peopleFilterHelper.filterPeopleByName(this.props.items, filterText);
    this.setState({
      filterText,
      filteredItems,
    });
  }

  render() {
    const content = this.getContent();
    return (
      <PeopleTemplate peopleFilter={this.handleFilter}>
        {content}
      </PeopleTemplate>
    );
  }
}

PeopleScreen.propTypes = {
  error: PropTypes.bool.isRequired,
  inProgress: PropTypes.bool.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  peopleFetch: PropTypes.func.isRequired,
};

const mapStateToProps = ({ people }) => ({
  ...people,
});

const mapDispatchToProps = dispatch => ({
  peopleFetch: () => dispatch(peopleFetchAsync()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PeopleScreen);

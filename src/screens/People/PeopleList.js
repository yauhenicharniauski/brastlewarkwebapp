import React from 'react';
import PropTypes from 'prop-types';
import PersonItem from './PersonItem';

function PeopleList({ items }) {
  return (
    <table className="people-table table table-bordered table-striped table-hover">
      <thead className="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Age</th>
          <th scope="col">Weight</th>
          <th scope="col">Height</th>
        </tr>
      </thead>
      <tbody>
        {
          items.length
            ? items.map(person => PersonItem(person))
            : (<tr className="odd"><td valign="top" colSpan="6">No matching records found</td></tr>)
        }
      </tbody>
    </table>
  );
}

PeopleList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape).isRequired,
};

export default PeopleList;

import React from 'react';
import PropTypes from 'prop-types';

function People(props) {
  const {
    peopleFilter,
    children,
  } = props;

  return (
    <div className="people-screen">
      <h1>Brastlewark population</h1>

      <p>
         You can use space to separate name parts. Example: &apos;tobu qu&apos;.
         It shows Tobus Quickwhistle, Tobus Quickpiston, Tobus Feltorque, ...etc
      </p>


      <div className="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">

        <div className="input-group input-group-lg">
          <div className="input-group-prepend">
            <span className="input-group-text" id="inputGroup-sizing-lg">Search</span>
          </div>
          <input
            type="text"
            className="form-control"
            placeholder="Type something..."
            aria-describedby="inputGroup-sizing-lg"
            onChange={peopleFilter}
          />
        </div>
      </div>

      <div className="people-table-container">
        {children}
      </div>
    </div>
  );
}

People.propTypes = {
  children: PropTypes.node.isRequired,
  peopleFilter: PropTypes.func.isRequired,
};

export default People;

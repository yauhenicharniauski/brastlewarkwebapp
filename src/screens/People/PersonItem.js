import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function PersonItem({
  id, name, age, weight, height,
}) {
  return (
    <tr key={id}>
      <td>{id}</td>
      <td><Link to={`/person/${id}`}>{name}</Link></td>
      <td>{age}</td>
      <td>{weight}</td>
      <td>{height}</td>
    </tr>
  );
}

PersonItem.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  age: PropTypes.any,
  weight: PropTypes.any,
  height: PropTypes.any,
};

PersonItem.defaultProps = {
  id: 0,
  name: '',
  age: '-',
  weight: '-',
  height: '-',
};

export default PersonItem;

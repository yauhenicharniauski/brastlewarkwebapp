import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './Person.scss';

function PersonErrorTemplate({ error }) {
  return (
    <div className="person-screen">
      <h1>Person</h1>
      <p>
        {error}
      </p>
      <Link className="btn btn-primary" to="/people">Go to people list</Link>
    </div>
  );
}

PersonErrorTemplate.propTypes = {
  error: PropTypes.string.isRequired,
};

export default PersonErrorTemplate;

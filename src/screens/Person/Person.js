import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import PersonTemplate from './PersonTemplate';
import peopleFilterHelper from '../../js/utils/peopleFilterHelper';
import { peopleFetchAsync } from '../../js/actions/people';
import Loading from '../../components/Loading/Loading';
import PersonErrorTemplate from './PersonErrorTemplate';

const initialState = {
  id: null,
  items: [],
  person: null,
};

class PersonScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      id: parseInt(props.match.params.id, 10),
    };
  }

  componentDidMount() {
    this.props.peopleFetch();
  }

  static getDerivedStateFromProps(props, state) {
    if (props.items === state.items) {
      return null;
    }

    return {
      items: props.items,
      person: peopleFilterHelper.getPersonById(props.items, state.id),
    };
  }

  render() {
    if (this.props.error) {
      return (<PersonErrorTemplate error="Can't get user from server. Try visit this page later." />);
    }

    if (this.props.inProgress) {
      return (<Loading />);
    }

    if (!this.state.person) {
      return (<PersonErrorTemplate error="Sorry, person was not found." />);
    }

    console.log('person', this.state.person);
    return (<PersonTemplate person={this.state.person} />);
  }
}

PersonScreen.propTypes = {
  match: PropTypes.object.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  error: PropTypes.bool.isRequired,
  inProgress: PropTypes.bool.isRequired,
  peopleFetch: PropTypes.func.isRequired,
};

const mapStateToProps = ({ people }) => ({
  ...people,
});

const mapDispatchToProps = dispatch => ({
  peopleFetch: () => dispatch(peopleFetchAsync()),
});


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PersonScreen));

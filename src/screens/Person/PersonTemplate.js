import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './Person.scss';

function PersonTemplate(props) {
  const {
    thumbnail, name, age, height, weight, friends,
  } = props.person;

  return (
    <div className="person-screen">
      <h1>{name}</h1>

      <div className="user-photo">
        <img alt={`${name} photo`} src={thumbnail} />
      </div>

      <p />

      <table className="people-table table table-bordered table-hover">
        <tbody>
          <tr>
            <td>Age</td>
            <td>{age}</td>
          </tr>
          <tr>
            <td>Height</td>
            <td>{weight}</td>
          </tr>
          <tr>
            <td>Weight</td>
            <td>{height}</td>
          </tr>
          <tr>
            <td>Firends</td>
            <td>
              {
            friends && friends.length
              ? <ul>{friends.map(friend => <li key={friend}>{friend}</li>)}</ul>
              : 'No frineds'
          }
            </td>
          </tr>
        </tbody>
      </table>


      <Link className="btn btn-primary" to="/people">Go to people list</Link>
    </div>
  );
}

PersonTemplate.propTypes = {
  person: PropTypes.object.isRequired,
};

export default PersonTemplate;

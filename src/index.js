import 'jquery/dist/jquery.slim';
import 'bootstrap/dist/js/bootstrap.bundle';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/App/App';
import initDependencies from './injector.config';
import store from './js/store';

import 'bootstrap/dist/css/bootstrap.min.css';
import './css/main.css';
import './scss/main.scss';


initDependencies();

ReactDOM.render(
  (<Provider store={store}><App /></Provider>), document.getElementById('root'),
);


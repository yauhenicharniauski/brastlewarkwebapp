import injector, { DI_DATA_SERVICE } from './js/utils/injector';
import createRealServerApi from './js/utils/serverApi/createRealServerApi';
import createDataServiceWithCache from './js/utils/dataService/createDataServiceWithCache';
// import createDataService from './js/utils/dataService/createDataService';
import { MINUTE } from './js/utils/time';

export default function initDependencies() {
  const serverApi = createRealServerApi();
  const dataService = createDataServiceWithCache({ serverApi, queryCachingTime: MINUTE });

  injector.register({
    key: DI_DATA_SERVICE,
    value: dataService,
  });
}

import React from 'react';

import './Adverts.scss';


function Adverts() {
  return (
    <ul className="app-adverts">
      <li>
        <h3>BitBucket respository</h3>
        <p>
          Would you like to see sources?
        </p>
        <a className="header-btn" href="https://bitbucket.org/yauhenicharniauski/brastlewarkwebapp" target="_blank" rel="noopener noreferrer">
        Go to BitBucket
        </a>
      </li>
    </ul>
  );
}

export default Adverts;

import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Home from '../../screens/Home/Home';
import People from '../../screens/People/People';
import About from '../../screens/About/About';

import './Main.scss';
import Person from '../../screens/Person/Person';

class App extends Component {
  render() {
    return (
      <div className="app-main">
        <Route exact path="/" component={Home} />
        <Route exact path="/people" component={People} />
        <Route exact path="/person/:id" component={Person} />
        <Route path="/about" component={About} />
      </div>
    );
  }
}

export default App;

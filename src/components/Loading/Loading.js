import React from 'react';
import './Loading.scss';

export default function () {
  return (
    <div className="loading-panel">
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  );
}

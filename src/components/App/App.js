import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Main from '../Main/Main';
import Header from '../Header/Header';
import Adverts from '../Adverts/Adverts';
import Footer from '../Footer/Footer';

import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="app">
        <BrowserRouter>
          <div className="app-container">
            <Header />
            <div className="app-body">
              <Main />
              <Adverts />
            </div>
            <Footer />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;

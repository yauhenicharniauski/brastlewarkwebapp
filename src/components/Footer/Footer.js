import React from 'react';

import './Footer.scss';


function Footer() {
  return (
    <div className="app-footer">
      &copy; 2019 powered by Eugen Charniauski
    </div>
  );
}

export default Footer;

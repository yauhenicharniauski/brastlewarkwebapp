import React from 'react';
import './Error.scss';

export default function () {
  return (
    <div className="error-panel">
      <div className="alert alert-danger" role="alert">
        <strong>Oh sorry!</strong>
        Error happens. Try reload this page later.
      </div>
    </div>
  );
}

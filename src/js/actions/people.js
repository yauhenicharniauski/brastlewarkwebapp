export const PEOPLE_FETCH_ASYNC = 'PEOPLE_FETCH_ASYNC';
export const PEOPLE_FETCH_SUCCESS = 'PEOPLE_SUCCESS';
export const PEOPLE_FETCH_ERROR = 'PEOPLE_FETCH_ERROR';

export function peopleFetchAsync() {
  return {
    type: PEOPLE_FETCH_ASYNC,
    payload: null,
  };
}

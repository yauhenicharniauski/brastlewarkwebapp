import { sleep, SECOND } from '../time';

export default function createFakeServerApi({ delay = SECOND }) {
  return {
    getPeople: async () => {
      if (delay) {
        await sleep(delay);
      }

      return [
        { id: 0, name: 'David' },
        { id: 1, name: 'Eugen' },
        { id: 2, name: 'Tommy' },
      ];
    },
  };
}

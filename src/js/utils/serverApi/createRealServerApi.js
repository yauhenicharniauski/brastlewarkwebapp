import { sleep } from '../time';

const GET_PEOPLE_REQUEST_URL = 'https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json';

async function getPeople() {
  try {
    // add short delay to demonstrate spinner
    await sleep(300);
    const res = await fetch(GET_PEOPLE_REQUEST_URL);
    const { Brastlewark: items } = await res.json();
    return items;
  } catch (err) {
    console.log(err);
    throw err;
  }
}

export default function createRealServerApi() {
  return {
    getPeople,
  };
}

export default function createDataService({ serverApi }) {
  const api = serverApi;
  return {
    getPeople: api.getPeople,
    clearCache: async () => {},
  };
}

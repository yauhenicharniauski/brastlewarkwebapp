import { MINUTE } from '../time';

const DEFAULT_CACHING_TIME = 5 * MINUTE;

export default function createDataServiceWithCache({
  serverApi,
  queryCachingTime = DEFAULT_CACHING_TIME,
}) {
  let cacheTime = {};
  let cache = {};

  const queryOrCached = async (key) => {
    const obj = cache[key];
    const time = cacheTime[key];
    if (!obj || !time || time < Date.now()) {
      cache[key] = await serverApi[key]();
      cacheTime[key] = Date.now() + queryCachingTime;
    }
    return cache[key];
  };

  return {
    getPeople: async () => queryOrCached('getPeople'),
    clearCache: async () => {
      cacheTime = {};
      cache = {};
    },
  };
}

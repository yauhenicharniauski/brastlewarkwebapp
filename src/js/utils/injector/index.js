export const DI_DATA_SERVICE = 'DI_DATA_SERVICE';

let deps = {};

function register({ key, value }) {
  deps[key] = value;
}

function clear() {
  deps = {};
}

export default {
  register,
  clear,
  getDataService: () => deps[DI_DATA_SERVICE],
};

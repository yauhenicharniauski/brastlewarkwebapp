export const SECOND = 1000;
export const MINUTE = 60 * SECOND;
export const sleep = delay => new Promise(res => setTimeout(res, delay));

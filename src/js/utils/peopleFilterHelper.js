function filterPeopleByName(items, expression = '') {
  /*
  * filter items by name property
  * experssion examples: 'Davi Ushe', 'Eugen', 'Jacson Michael'
  */

  const handled = expression.trim().toLowerCase();
  if (!handled) {
    return items;
  }

  const parts = handled.split(' ');
  let result = items;

  parts.forEach((part) => {
    if (!part) {
      return;
    }
    result = result.filter(item => item.name && item.name.toLowerCase().indexOf(part) > -1);
  });

  return result;
}

function getPersonById(items, id) {
  const result = items.filter(item => item.id === id);
  if (result.length) {
    return result[0];
  }
  return null;
}

export default {
  getPersonById,
  filterPeopleByName,
};



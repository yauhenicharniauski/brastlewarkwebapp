import { PEOPLE_FETCH_ASYNC, PEOPLE_FETCH_SUCCESS, PEOPLE_FETCH_ERROR } from '../actions/people';

const initialState = {
  items: [],
  success: false,
  inProgress: false,
  error: false,
};

export default function (state = initialState, action) {
  const { payload, type } = action;

  switch (type) {
    case PEOPLE_FETCH_ASYNC:
      return {
        ...initialState,
        inProgress: true,
      };

    case PEOPLE_FETCH_SUCCESS:
      return {
        ...initialState,
        items: [...payload],
        success: true,
      };

    case PEOPLE_FETCH_ERROR:
      return {
        ...initialState,
        error: true,
      };

    default:
      return state;
  }
}

import { call, put, takeLatest } from 'redux-saga/effects';
import { PEOPLE_FETCH_ASYNC, PEOPLE_FETCH_SUCCESS, PEOPLE_FETCH_ERROR } from '../actions/people';
import injector from '../utils/injector';

function* peopleFetch() {
  try {
    const dataService = injector.getDataService();
    const data = yield call(dataService.getPeople);
    yield put({ type: PEOPLE_FETCH_SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: PEOPLE_FETCH_ERROR });
  }
}

export default function* watchPeopleFetch() {
  yield takeLatest(PEOPLE_FETCH_ASYNC, peopleFetch);
}

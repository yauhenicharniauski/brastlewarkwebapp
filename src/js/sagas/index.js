import { all } from 'redux-saga/effects';
import watchPeopleFetch from './watchPeopleFetch';


export default function* rootSaga() {
  yield all([
    watchPeopleFetch(),
  ]);
}

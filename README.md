Run 'npm run dev', to execute solution in dev mode
Run 'npm run build', to collect js/css/imgs etc files and put them to ./dist folder
Run 'npm run lint', to validate js code
Run 'npm run fix', to prerrify code and validate js code
Run 'npm run test', to run the module's unit tests


Should we actually use REDUX & Middleware?

This is not real solution. This is just a demo that shows how we can use diffrenet technologies.
Actually current solution shouldn't use Store. Cause we don't have application state.
If we don't use store, then we have simple components, that contain only state = { loading, error, data },
componentDidMount with dataService.getPeople call and render method. No need implement getDeriveStateFromProps, store, reducers, actions and sagas.
I used Redux Store because I would like to demonstrate how we can implement Middleware with Async calls. For this I used redux-saga library.

How we can cache data?
I created DataService object that is a wrapper. And it wraps serverApi calls. Also I implemented cache inside DataService. We can improve this solution and use LocalStorega to cache data for a long time. I don't cache images cause browser already do this job.

Why do we have two similar classes ServerApi and DataService ?
ServerApi class is a facade, and it wraps ajax calls to real server. It doesn't have any logic.
If we would like create additional logic or add extra methods, for example getPersonById, we should use DataService
class. Because real server doesnt support getPersonById method. 



import peopleFilterHelper from '../src/js/utils/peopleFilterHelper';

const fakeData = [
  { id: 0, name: 'David' },
  { id: 1, name: 'Eugen' },
  { id: 2, name: 'Tommy Hilfigier' },
];

test('peopleFilterHelper.filterPeopleByName: no data', async () => {
  let res = peopleFilterHelper.filterPeopleByName([], 'daviD');
  expect(res.length).toBe(0);

  res = peopleFilterHelper.filterPeopleByName(fakeData, '');
  expect(res.length).toBe(fakeData.length);
});


test('peopleFilterHelper.filterPeopleByName: case sensetive filter test', async () => {
  const res = peopleFilterHelper.filterPeopleByName(fakeData, 'daviD');
  expect(res.length).toBe(1);
  expect(res[0].name).toBe('David');
});

test('peopleFilterHelper.filterPeopleByName: test filter with double expression', async () => {
  let res = peopleFilterHelper.filterPeopleByName(fakeData, 'tomm hilf');
  expect(res.length).toBe(1);
  expect(res[0].name).toBe('Tommy Hilfigier');

  res = peopleFilterHelper.filterPeopleByName(fakeData, 'hilF tomM');
  expect(res.length).toBe(1);
  expect(res[0].name).toBe('Tommy Hilfigier');
});

test('peopleFilterHelper.getPersonById', async () => {
  let res = peopleFilterHelper.getPersonById(fakeData, 123);
  expect(res).toBe(null);

  res = peopleFilterHelper.getPersonById([], 123);
  expect(res).toBe(null);

  res = peopleFilterHelper.getPersonById(fakeData, 1);
  expect(!!res).toBe(true);
  expect(res.name).toBe('Eugen');
});

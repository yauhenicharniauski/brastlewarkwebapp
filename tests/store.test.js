import injector, { DI_DATA_SERVICE } from '../src/js/utils/injector';
import createDataService from '../src/js/utils/dataService/createDataService';
import createDataServiceWithCache from '../src/js/utils/dataService/createDataServiceWithCache';
import store, { runAction } from '../src/js/store';
import { peopleFetchAsync } from '../src/js/actions/people';
import { sleep } from '../src/js/utils/time';

const data1 = [
  { id: 0, name: 'David' },
  { id: 1, name: 'Eugen' },
];

const data2 = [
  { id: 0, name: 'David' },
  { id: 1, name: 'Eugen' },
  { id: 2, name: 'Tommy Hilfigier' },
];

let fakeServerApi = (function () {
  let items;
  let error;

  return {
    setError: (mode) => { error = mode;},
    setItems: (newItems)=> { items = [ ...newItems]; },
    getPeople: async () => { 
      if (error) throw new Error('');
      return items; 
    }
  }
}());


test('store: test peopleFetchAsync action', async () => {

  fakeServerApi.setItems(data1);
  let dataService = createDataService({ serverApi: fakeServerApi });
  injector.register({ key: DI_DATA_SERVICE, value: dataService });


  runAction(peopleFetchAsync());
  await sleep(200);
  expect(store.getState().people.items.length).toBe(2);
  expect(store.getState().people.success).toBe(true);
  expect(store.getState().people.inProgress).toBe(false);
  expect(store.getState().people.error).toBe(false);

});


test('store: test peopleFetchAsync action with cache', async () => {

  fakeServerApi.setItems(data1);
  let dataService = createDataServiceWithCache({ serverApi: fakeServerApi, queryCachingTime: 1000 });
  injector.register({ key: DI_DATA_SERVICE, value: dataService });


  runAction(peopleFetchAsync());
  await sleep(100);
  expect(store.getState().people.items.length).toBe(2);
  expect(store.getState().people.success).toBe(true);
  

  fakeServerApi.setItems(data2);

  await sleep(100);
  runAction(peopleFetchAsync());
  await sleep(100);
  expect(store.getState().people.items.length).toBe(2); // cache works

  await sleep(2000);

  await dataService.getPeople();

  runAction(peopleFetchAsync());
  await sleep(100);

  expect(store.getState().people.items.length).toBe(3); 
  expect(store.getState().people.success).toBe(true); 
});


test('store: test peopleFetchAsync, emulate server error', async () => {

  fakeServerApi.setItems(data1);
  let dataService = createDataService({ serverApi: fakeServerApi });
  injector.register({ key: DI_DATA_SERVICE, value: dataService });

  runAction(peopleFetchAsync());
  await sleep(100);
  expect(store.getState().people.success).toBe(true);
  expect(store.getState().people.error).toBe(false);

  fakeServerApi.setError(true);

  runAction(peopleFetchAsync());
  await sleep(100);
  expect(store.getState().people.items.length).toBe(0);
  expect(store.getState().people.success).toBe(false);
  expect(store.getState().people.error).toBe(true);

});
